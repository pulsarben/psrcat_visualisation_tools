#!/usr/bin/python

from __future__ import print_function
import numpy as np
import matplotlib.pyplot as plt
import sys
import subprocess
import argparse
import tempfile


class plot_ppdot(object):

    def __init__(self, pulsar=False, saveplot=False):

        # Update class variables
        self.pulsar   = pulsar   # Name of pulsar
        self.saveplot = saveplot # Save plot to disk

    def run_psrcat_all(self):
        all_psr = ['psrcat', '-x', '-c', 'P0 P1 PB ASSOC']
        proc = subprocess.Popen(all_psr, stdout=subprocess.PIPE)
        all_psr = proc.stdout.read()
        return all_psr

    def formulate_psrcat(self):
        # All pulsars P and Pdot
        P0 = []
        P1 = []
        # Binary pulsars P and Pdot
        P0_bin = []
        P1_bin = []
        # Pulsars with SNRs P and Pdot
        P0_snr = []
        P1_snr = []
 
        # For each line in the psrcat tables,
        lines = self.psrcat_out.split('\n')
        for line in lines:
             if line:
                 col = line.split()
                 # If it has a P and Pdot value, 
                 if ("*" not in col[3]):
                     P0.append(float(col[0]))
                     P1.append(float(col[3]))
                     if ("*" not in col[6]):
                         P0_bin.append(float(col[0]))
                         P1_bin.append(float(col[3]))
                     if ("SNR" in col[9]):
                         P0_snr.append(float(col[0]))
                         P1_snr.append(float(col[3]))
        return P0, P1, P0_bin, P1_bin, P0_snr, P1_snr

    @staticmethod
    def tfile(pulsar):
        with tempfile.NamedTemporaryFile(delete=False) as f:
            f.write(pulsar)
            return f.name

    def this_psrcat(self, infile):
       psrcat_in = ['psrcat', '-x', '-c', 'P0 P1', '-psr', infile]
       psrcat_call = subprocess.Popen(psrcat_in, stdout=subprocess.PIPE)
       psrcat_out = psrcat_call.stdout.read() 

       disjoin = psrcat_out.split()
       period = disjoin[0]
       if period == "WARNING:":
           print("\n{} not found in database\n".format(self.pulsar))
           sys.exit(9)
       period_div = disjoin[3]
       print("\nParameters for {}".format(self.pulsar))
       print("Period = {} s".format(period))
       print("Period derivative = {}\n".format(period_div))
       return period, period_div

    def do_plot(self, pulsar):
        P0s = self.ppdots[0]
        P1s = self.ppdots[1]
        P0_bin = self.ppdots[2]
        P1_bin = self.ppdots[3]
        P0_snr = self.ppdots[4]
        P1_snr = self.ppdots[5]

        plt.plot(P0_bin, P1_bin, 'bo', label="Binary")
        plt.plot(P0_snr, P1_snr, 'ro', label="SNR")
        plt.plot(P0s, P1s, 'k*')
        plt.xscale('log')
        plt.yscale('log')
        plt.show()


    def plot(self):

        # Get ppdot of all pulsars from psrcat
        # as a table
        self.psrcat_out = self.run_psrcat_all()

        # Formulate these as arrays
        self.ppdots = self.formulate_psrcat()

        # Do we have a pulsar we want to highlight?
        if self.pulsar:

            # Stick the name of the pulsar in a temp file
            infile = self.tfile(self.pulsar)

            # Get P and Pdot for the pulsar we're interested in
            this_p_and_pdot = self.this_psrcat(infile)
            self.period = this_p_and_pdot[0]
            self.period_deriv = this_p_and_pdot[1]

        self.do_plot(self.pulsar)         

            

def show_atrr():
    '''
    Prints list of attributes we can use
    to compare a pulsar to the population
    '''
    print("\nBelow is a list of attributes you can pass with the -a option\n")

    print("ppdot\t\t P-pdot diagram")
    print("edot\t\t Spin-down energy histogram")
    print("rlum\t\t Radio luminosity histogram")
    print("s1400\t\t Flux density at 1400 MHz histogram")
    print("galpos\t\t Galactic position")
    print("dm\t\t Dispersion measure histogram")
    print("bsurf\t\t Surface magnetic field histogram")
    print("pm\t\t Proper motion histogram")

def main():

    parser = argparse.ArgumentParser(description='Library to compare pulsar properties with those of the population')
    parser.add_argument('-p','--pulsar', help='Name of pulsar', required=False)
    parser.add_argument('-s', '--saveplot', help="Save a copy of the plot to disk", action='store_true')
    parser.add_argument('-a', '--attr', help='Attribute to compare (see -H option)', required=False)
    parser.add_argument('-H', '--attributes_list', help='Show list of available attributes', required=False, action='store_true')

    args = parser.parse_args()

    if args.attributes_list or not args.attr:
        show_atrr()
        sys.exit(9)

    if args.attr == "ppdot":
        plot_ppdot(args.pulsar, args.saveplot).plot()
    elif args.attr == "edot":
        pass
    elif args.attr == "rlum":
        pass
    elif args.attr == "s1400":
        pass
    elif args.attr == "galpos":
        pass
    elif args.attr == "dm":
        pass
    elif args.attr == "bsurf":
        pass
    elif args.attr == "pm":
        pass
    else:
        print("Invalid attribute passed to -a")
        sys.exit(9)

if __name__ == '__main__':
    main()
